import os
import sys
from builtins import str


appname = str(sys.argv[1])

def newapp():
    # Create New Django app
    os.system(f'python manage.py startapp {appname}')

    # PATHS
    my_templates = '/Users/leelaing/Development/templates'
    current_path = os.path.abspath(str())
    app_folder = f'{current_path}/{appname}'
    templates_folder = f'{app_folder}/templates'
    static_folder = f'{app_folder}/static'
    media_folder = f'{app_folder}/media'

    # FILENAMES
    base = 'base_django_materializecss.html'
    home = 'django_app_home.html'
    css = 'django_app_css.sass'
    js = 'django_app_js.js'
    urls = 'django_app_urls.py'
    views = 'django_app_views.py'

    def create_folder(folder):
        if folder:
            os.makedirs(folder)
        else:
            print('Something Already exists!')

    try:
        create_folder(app_folder)
    except FileExistsError:
        print(f'{app_folder} already exists!')
    finally:
        create_folder(f'{templates_folder}/{appname}')
        create_folder(f'{media_folder}/{appname}')
        create_folder(f'{static_folder}/{appname}/css')
        create_folder(f'{static_folder}/{appname}/js')

    def create_file(folder, file_name, new_name):
        with open(f'{my_templates}/{file_name}') as my_template:
            with open(f'{folder}/{new_name}', 'w') as my_new_template:
                for line in my_template:
                    my_new_template.write(line)

    create_file(f'{templates_folder}/{appname}', base, 'base.html')
    create_file(f'{templates_folder}/{appname}', home, 'home.html')
    create_file(f'{static_folder}/{appname}/css', css, f'{appname}.sass')
    create_file(f'{static_folder}/{appname}/js', js, f'{appname}.js')
    create_file(f'{current_path}/{appname}', urls, 'urls.py')
    create_file(f'{current_path}/{appname}', views, 'views.py')

    def replace_name(location, file_to_copy, file_to_save):
        fin = open(f'{my_templates}/{file_to_copy}', 'r')
        fout = open(f'{location}/{appname}/{file_to_save}', 'w')

        for line in fin:
            fout.write(line.replace('!APPNAME!', appname))

        fin.close()
        fout.close()

    replace_name(f'{static_folder}/', js, f'js/{appname}.js')
    replace_name(f'{static_folder}/', css, f'css/{appname}.sass')
    replace_name(templates_folder, base, 'base.html')
    replace_name(templates_folder, home, 'home.html')
    replace_name(current_path, urls, 'urls.py')
    replace_name(current_path, views, 'views.py')

    print(f'"{appname}" App Directory has been created for you successfully!')


if __name__ == "__main__":
    newapp()

# newapp('test_app')
