# "New App" for Django/Python Project

## Includes:

- newapp (executable)
- newapp.py (source file)
- newapp.spec (pyinstaller file)

### Description

This script was created to eliminate the need to repetitively create the APP template, static, and media directories.

Along with the directories, each time a urls.py file is created and populated for the new app.

### Functions
- Adds new Django app through manage.py automatically.
- Adds templates, static (css and js), media directories to a new Django app that has been created.
- Copies base html, css, sass and python files and places them into the correct and Corresponding directories.
- Names the css & js files to be the same name as the app name.
- Ensures that the static links for the css & js files are properly pointing to the new files created.


### Usage

```bash
git clone https://gitlab.com/big2tinydev/python-projects.git <new name>
```

Added and alias so it can be run from any directory.

.bash_profile
```bash
alias newapp=<path-to-project-folder>
```

Also, you will need to modify the script variable "my_templates" to have the correct path that points to your template directory.

Executing...from any directory
```bash
newapp <AppName>
```
"AppName" must be the same name that your django app is named. (exact folder name)

### Future Plans

Hope to add to this to Programmatically for auto updating the main settings file if possible and if can be done safely. 

### Open to Suggestions

Please feel free to give suggestions or if this can be improved upon, I would love to also know this.

Reach me at:

big2tinydev@gmail.com

